package com.roronoadiogo.uol.desafio.services;

import com.roronoadiogo.uol.desafio.dto.ProductDTO;

import java.math.BigDecimal;
import java.util.List;

public interface ProductServices {

    /**
     * Cadastra um novo produto na base
     * @param product
     * @return productDto
     */
    ProductDTO addProduct(ProductDTO product);

    /**
     * Atualiza um novo produto na base
     * @param productNew
     * @return productDto
     */
    ProductDTO updateProduct(ProductDTO productNew);

    /**
     * Lista todos os produtos
     * @return List<ProductDto>
     */
    List<ProductDTO> getAllProducts();

    /**
     * Deleta um  produto da base
     * @param id
     */
    void deleteProductById(String id);

    /**
     * Encontra um produto na base
     * @param id
     * @return Optnal e lança exceção
     */
    ProductDTO findById(String id);

    /**
     * Encontra um produto na base com parametos
     * @param max valor menor ou igual o preco
     * @param min valor maior ou igual o preco
     * @param name texto no nome ou descrição
     * @return ProductDTO caso contrário, lança exceção
     */
    List<ProductDTO> findByNameDescriptionParams(String name, BigDecimal min, BigDecimal max);
}
