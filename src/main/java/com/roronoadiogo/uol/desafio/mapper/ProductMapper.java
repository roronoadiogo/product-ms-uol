package com.roronoadiogo.uol.desafio.mapper;

import com.roronoadiogo.uol.desafio.dto.ProductDTO;
import com.roronoadiogo.uol.desafio.model.Product;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product toProduct(ProductDTO dto);

    ProductDTO toDto(Product product);

    List<ProductDTO> toDto(List<Product> productList);

}
