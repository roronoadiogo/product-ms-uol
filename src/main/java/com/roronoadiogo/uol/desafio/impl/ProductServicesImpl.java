package com.roronoadiogo.uol.desafio.impl;

import com.roronoadiogo.uol.desafio.dto.ProductDTO;
import com.roronoadiogo.uol.desafio.exception.ResourceNotFoundRequestException;
import com.roronoadiogo.uol.desafio.mapper.ProductMapper;
import com.roronoadiogo.uol.desafio.model.Product;
import com.roronoadiogo.uol.desafio.repository.ProductRepository;
import com.roronoadiogo.uol.desafio.services.ProductServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ProductServicesImpl implements ProductServices {

    private static final Logger log = LoggerFactory.getLogger(ProductServicesImpl.class);

    private ProductRepository repository;
    private ProductMapper productMapper;

    public ProductServicesImpl(ProductRepository repository, ProductMapper productMapper) {
        this.repository = repository;
        this.productMapper = productMapper;
    }

    @Override
    public ProductDTO addProduct(ProductDTO productDTO) {
        log.info("Persistindo produto: {}", productDTO);
        return sameFlux(productDTO);

    }

    @Override
    public ProductDTO updateProduct(ProductDTO productNew) {
        log.info("Atualizando produto: {}", productNew);

        var product = repository.findById(productNew.getId()).orElseThrow(() -> new ResourceNotFoundRequestException("Not found resource"));
        product.setDescription(productNew.getDescription());
        product.setPrice(productNew.getPrice());
        product.setName(productNew.getName());

        return sameFlux(productMapper.toDto(product));
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        log.info("Buscando lista de produtos");
        return productMapper.toDto(repository.findAll());

    }

    @Override
    public void deleteProductById(String id) {
        log.info("Deletando o produto {}", id);
        var product = repository.findById(id).orElseThrow(() -> new ResourceNotFoundRequestException("Not Found resource"));
        repository.deleteById(product.getId());
    }

    @Override
    public ProductDTO findById(String id) {
        log.info("Encontrando o produto pelo id {}", id);
        var product = repository.findById(id).orElseThrow(() -> new ResourceNotFoundRequestException("Not Found resource"));
        return productMapper.toDto(product);
    }

    @Override
    public List<ProductDTO> findByNameDescriptionParams(String name, BigDecimal min, BigDecimal max) {
        log.info("Acionando os query params ");
        var product = repository.findByNameAndDescriptionAndPrice(min, max, name);
        return productMapper.toDto(product);
    }

    /**
     * Mesmo fluxo para atualizar e adicionar
     * intuito de apenas manter legibilidade no fonte
     * @param productDTO
     * @return product
     * */

    private ProductDTO sameFlux(ProductDTO productDTO){
        Product product = productMapper.toProduct(productDTO);
        product = repository.save(product);
        return productMapper.toDto(product);
    }
}
