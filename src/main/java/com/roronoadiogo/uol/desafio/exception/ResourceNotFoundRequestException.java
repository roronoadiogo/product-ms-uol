package com.roronoadiogo.uol.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundRequestException extends RuntimeException{
    public ResourceNotFoundRequestException(String message) {
        super(message);
    }
}
