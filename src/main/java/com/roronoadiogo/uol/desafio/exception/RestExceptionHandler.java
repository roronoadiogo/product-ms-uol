package com.roronoadiogo.uol.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseErrorAPI> handlerErrorValid(MethodArgumentNotValidException methodArgumentNotValidException) {

        var allFieldsErros = methodArgumentNotValidException.getFieldErrors();
        String fields = allFieldsErros.stream().map(FieldError::getField).collect(Collectors.joining(", "));
        String messages = allFieldsErros.stream().map(FieldError::getDefaultMessage).collect(Collectors.joining(", "));

        var response = new ResponseErrorAPI(400, "error in the fields: " + fields + " " + messages);
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ResponseErrorAPI> handlerErrorBadRequest(BadRequestException exception) {
        var response = new ResponseErrorAPI(400, exception.getMessage());
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(ResourceNotFoundRequestException.class)
    public ResponseEntity<ResponseErrorAPI> notFoundResource(ResourceNotFoundRequestException exception) {
        var response = new ResponseErrorAPI(404, exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

}
