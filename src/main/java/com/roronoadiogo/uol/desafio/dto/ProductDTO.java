package com.roronoadiogo.uol.desafio.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

public class ProductDTO {

    private String id;

    @NotNull(message = "This field is mandatory and not possible has null")
    @NotBlank(message = "This field is mandatory")
    private String name;

    @NotNull(message = "This field is mandatory and not possible has null")
    @NotBlank(message = "This field is mandatory")
    private String description;

    @NotNull(message = "This field is mandatory and not possible has null")
    @PositiveOrZero(message = "This field has must be higher or equal zero")
    private BigDecimal price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
