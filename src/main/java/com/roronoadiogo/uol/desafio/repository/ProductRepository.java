package com.roronoadiogo.uol.desafio.repository;

import com.roronoadiogo.uol.desafio.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, String> {

    Optional<Product> findById(String s);

    @Query("SELECT p FROM Product p WHERE (:nome is null or p.name LIKE %:nome% and p.description LIKE %:nome%) and " +
            "(:min_price is null or p.price >= :min_price) and (:max_price is null or p.price <= :max_price)")
    List<Product> findByNameAndDescriptionAndPrice(@Param("min_price")BigDecimal min,
                                                   @Param("max_price") BigDecimal max,
                                                   @Param("nome") String nome);

}
