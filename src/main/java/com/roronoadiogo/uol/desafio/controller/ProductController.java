package com.roronoadiogo.uol.desafio.controller;

import com.roronoadiogo.uol.desafio.dto.ProductDTO;
import com.roronoadiogo.uol.desafio.services.ProductServices;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class ProductController {

    private ProductServices productService;

    public ProductController(ProductServices productService) {
        this.productService = productService;
    }

    @ApiOperation("Create one new product in database")
    @PostMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> add(@RequestBody @Valid ProductDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.addProduct(dto));
    }

    @ApiOperation("Update one existing product in database")
    @PutMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> update(@PathVariable String id, @Valid @RequestBody ProductDTO dtoNew) {
        dtoNew.setId(id);
        return ResponseEntity.status(HttpStatus.OK).body(productService.updateProduct(dtoNew));
    }

    @ApiOperation("Return one product existing in database")
    @GetMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> findById(@PathVariable String id) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.findById(id));
    }

    @ApiOperation("Return a list of products")
    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDTO>> all() {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getAllProducts());
    }

    @ApiOperation("Search using Request Params")
    @GetMapping(value = "/products/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDTO>> searchParams(@RequestParam(required = false,
            name="min_price") BigDecimal min, @RequestParam(required = false, name="max_price") BigDecimal max,
            @RequestParam(required = false, name = "q") String q){
        return ResponseEntity.status(HttpStatus.OK).body(productService.findByNameDescriptionParams(q, min, max));
    }

    @ApiOperation("Delete one existing product in database")
    @DeleteMapping(value = "/products/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> delete(@PathVariable String id) {
        productService.deleteProductById(id);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

}
