# Product-MS | _Desafio técnico UOL_
Desenvolver uma API Rest para listagem de produtos

## Tecnologias usadas
- Spring REST
- Spring DATA
- H2 Database
- Java 11
- DTO como Map Struct
- Swagguer UI [API](http://localhost:9999/swagger-ui.html "link")